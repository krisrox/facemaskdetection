import json
import pathlib
import sys
import subprocess
from PyQt5 import QtCore
from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QIcon, QPixmap, QRegExpValidator
from PyQt5.QtWidgets import QWidget, QDesktopWidget, \
    QGridLayout, QPushButton, QLabel, QMainWindow, QAction, QMenuBar, QFileDialog, QVBoxLayout, QDialog, \
    QLineEdit, QSizePolicy, QProgressBar
from detect_mask_from_video import DetectMaskFromVideo
from detect_mask_from_image import DetectMaskFromImage
from Exceptions import FaceMaskException, ImageException, WebcamException

detectFromVideo = DetectMaskFromVideo()
detectFromImage = DetectMaskFromImage()


class AboutDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(AboutDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle("About")
        self.setWindowIcon(QIcon('data/icons/about.png'))
        self.setFixedSize(600, 250)

        self.lbl_top = QLabel("<h2>Graduate work at bachelor of engineering</h2>")
        self.lbl_top.setAlignment(QtCore.Qt.AlignCenter)

        self.lbl_center = QLabel("<b>DETECTION OF PERSONS "
                                 "<br>WITHOUT A PROTECTIVE MASK "
                                 "<br>ON THE FACE USING DEEP LEARNING</b>")
        self.lbl_center.setAlignment(QtCore.Qt.AlignCenter)

        self.lbl_bottom = QLabel("Author\nMichał Staruch")
        self.lbl_bottom.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.lbl_top)
        self.layout.addWidget(self.lbl_center)
        self.layout.addWidget(self.lbl_bottom)
        self.setLayout(self.layout)


class ErrorDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(ErrorDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle("Error")
        self.setWindowIcon(QIcon('data/icons/error.png'))
        self.setFixedSize(400, 150)

        self.btn_close = QPushButton("OK")
        self.btn_close.clicked.connect(lambda: self.close())


class InfoDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(InfoDialog, self).__init__(*args, **kwargs)

        self.setWindowTitle("Information")
        self.setWindowIcon(QIcon('data/icons/info.png'))
        self.setFixedSize(400, 150)

        self.btn_close = QPushButton("OK")
        self.btn_close.clicked.connect(lambda: self.close())


class SetDefaultModelDialog(InfoDialog):
    def __init__(self, *args, **kwargs):
        super(SetDefaultModelDialog, self).__init__(*args, **kwargs)

        self.lbl_info = QLabel("Loading default trained model.")
        self.lbl_info.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.lbl_info)
        self.layout.addWidget(self.btn_close)
        self.setLayout(self.layout)


class FaceMaskNotFoundDialog(ErrorDialog):
    def __init__(self, *args, **kwargs):
        super(FaceMaskNotFoundDialog, self).__init__(*args, **kwargs)

        self.lbl_error = QLabel("There was an error detecting the face mask.")
        self.lbl_error.setAlignment(QtCore.Qt.AlignCenter)

        self.lbl_tip = QLabel("Please try again with another image.")
        self.lbl_tip.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.lbl_error)
        self.layout.addWidget(self.lbl_tip)
        self.layout.addWidget(self.btn_close)
        self.setLayout(self.layout)


class VideoErrorDialog(ErrorDialog):
    def __init__(self, *args, **kwargs):
        super(VideoErrorDialog, self).__init__(*args, **kwargs)
        self.setFixedSize(500, 150)

        self.lbl_error = QLabel("There was an error detecting the face mask using webcam.")
        self.lbl_error.setAlignment(QtCore.Qt.AlignCenter)

        self.lbl_tip = QLabel("Please try again.")
        self.lbl_tip.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.lbl_error)
        self.layout.addWidget(self.lbl_tip)
        self.layout.addWidget(self.btn_close)
        self.setLayout(self.layout)


class ImageNotFoundDialog(ErrorDialog):
    def __init__(self, *args, **kwargs):
        super(ImageNotFoundDialog, self).__init__(*args, **kwargs)

        self.lbl_error = QLabel("There was an error loading image.")
        self.lbl_error.setAlignment(QtCore.Qt.AlignCenter)

        self.lbl_tip = QLabel("Please try again.")
        self.lbl_tip.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.lbl_error)
        self.layout.addWidget(self.lbl_tip)
        self.layout.addWidget(self.btn_close)
        self.setLayout(self.layout)


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.menubar = QMenuBar()
        self.setMinimumSize(1000, 700)
        self.setWindowTitle('Face Mask Detector')
        self.setWindowIcon(QIcon('data/icons/face_mask.png'))
        self.__center()
        self.show()

        self.plate = "None"
        self.path = ""
        self.model_path = ""
        self.photo = QLabel("Your photo will appear here after uploading")
        self.photo.setScaledContents(True)
        self.photo.setAlignment(QtCore.Qt.AlignCenter)

        self.btn_training = QPushButton("Training")
        self.btn_open = QPushButton("Open a photo")
        self.btn_read_from_photo = QPushButton("Detect from photo")
        self.btn_detect_from_video = QPushButton("Detect from video")
        self.btn_set_model = QPushButton("Set alternate model")

        self.__init_ui()

    def __init_ui(self) -> None:
        layout = QGridLayout()
        self.statusBar().showMessage("Starting the application.")

        layout.addWidget(self.__init_menubar(), 0, 0, 1, 8)
        layout.addWidget(self.btn_training, 1, 0, 1, 2)
        layout.addWidget(self.btn_open, 4, 0, 1, 2)
        layout.addWidget(self.btn_read_from_photo, 6, 0, 1, 2)
        layout.addWidget(self.btn_detect_from_video, 7, 0, 1, 2)
        layout.addWidget(self.btn_set_model, 15, 0, 1, 2)
        layout.addWidget(self.photo, 1, 2, 15, 6)

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)
        central_widget.setLayout(layout)

        self.__init_events()
        self.statusBar().showMessage("Application ready")

    def __init_menubar(self) -> QMenuBar:
        menu_file = self.menubar.addMenu('File')

        act_open = QAction(QIcon('data/icons/open.png'), '&Open image', self)
        act_open.setShortcut('Ctrl+O')
        act_open.setStatusTip('Open image')
        act_open.triggered.connect(lambda: load_photo(self))
        menu_file.addAction(act_open)
        menu_file.addSeparator()

        act_read = QAction(QIcon('data/icons/read.png'), '&Load model', self)
        act_read.setShortcut('Ctrl+R')
        act_read.setStatusTip('Load model')
        act_read.triggered.connect(lambda: choice_alternate_model(self))
        menu_file.addAction(act_read)
        menu_file.addSeparator()

        act_exit = QAction(QIcon('data/icons/exit.png'), '&Exit', self)
        act_exit.setShortcut('Ctrl+Q')
        act_exit.setStatusTip('Exit application')
        act_exit.triggered.connect(lambda: sys.exit())
        menu_file.addAction(act_exit)

        menu_help = self.menubar.addMenu("Help")

        act_about = QAction(QIcon('data/icons/about.png'), '&About', self)
        act_about.setShortcut('Ctrl+A')
        act_about.setStatusTip('About')
        act_about.triggered.connect(lambda: self.__about())
        menu_help.addAction(act_about)

        return self.menubar

    def __init_events(self) -> None:
        self.btn_training.clicked.connect(lambda: train(self))
        self.btn_open.clicked.connect(lambda: load_photo(self))
        self.btn_read_from_photo.clicked.connect(lambda: detect_from_photo(self))
        self.btn_detect_from_video.clicked.connect(lambda: detect_from_video(self))
        self.btn_set_model.clicked.connect(lambda: choice_alternate_model(self))

    def __center(self) -> None:
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())

    def __about(self) -> None:
        dlg = AboutDialog(self)
        dlg.exec_()


def train(window: Window) -> None:
    window.statusBar().showMessage("Training neural network...")
    subprocess.call([r'run_training.bat'])
    window.statusBar().showMessage("Network trained!")


def load_photo(window: Window):
    window.statusBar().showMessage("File explorer opened")

    path, _filter = QFileDialog.getOpenFileName(
        parent=window,
        caption=window.tr("Open file"),
        directory='images',
        filter=window.tr("Image files (*.png *.jpg)"))
    window.photo.setPixmap(QPixmap(path))
    window.path = path
    window.statusBar().showMessage("Image loaded")


def detect_from_photo(window: Window) -> None:
    window.statusBar().showMessage("Detecting face mask from the image... Press Q to exit")

    if not window.model_path:
        dlg = SetDefaultModelDialog(window)
        dlg.exec_()

    try:
        detectFromImage.detect_mask(window.path, window.model_path)
        window.statusBar().showMessage("Application ready")

    except ImageException:
        dlg = ImageNotFoundDialog(window)
        window.statusBar().showMessage("Application ready")
        dlg.exec_()
    except FaceMaskException:
        dlg = FaceMaskNotFoundDialog(window)
        window.statusBar().showMessage("Application ready")
        dlg.exec_()


def detect_from_video(window: Window) -> None:
    window.statusBar().showMessage("Detecting face mask from the video... Press Q to exit")

    if not window.model_path:
        dlg = SetDefaultModelDialog(window)
        dlg.exec_()

    try:
        detectFromVideo.detect_mask(window.model_path)
        window.statusBar().showMessage("Application ready")
    except:
        dlg = VideoErrorDialog(window)
        window.statusBar().showMessage("Application ready")
        dlg.exec_()


def choice_alternate_model(window: Window) -> None:
    window.statusBar().showMessage("File explorer opened")

    path, _filter = QFileDialog.getOpenFileName(
        parent=window,
        caption=window.tr("Open file"),
        directory='result',
        filter=window.tr("Model files (*.model)"))
    window.model_path = path
    window.statusBar().showMessage("Trained model loaded")
