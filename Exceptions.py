class FaceMaskException(Exception):
    def __init__(self, message):
        super(FaceMaskException, self).__init__(message)


class WebcamException(Exception):
    def __init__(self, message):
        super(WebcamException, self).__init__(message)


class ImageException(Exception):
    def __init__(self, message):
        super(ImageException, self).__init__(message)
