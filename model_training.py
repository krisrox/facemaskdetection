import tensorflow.keras.layers.experimental.preprocessing
from tensorflow.keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from tensorflow.keras.layers import Dropout, Dense, Flatten, Conv2D, MaxPooling2D, BatchNormalization
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from fastprogress import master_bar, progress_bar
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import os
import time
from datetime import datetime
from warnings import filterwarnings

filterwarnings("ignore")

IMAGE_SIZE = (224, 224)
INITIAL_LEARNING_RATE = 1e-4
EPOCHS = 50
BATCH_SIZE = 32
DATASET_NAME = "dataset"
TRAINED_MODEL_LOCALIZATION = "result/trained.model"
PLOT_LOCALIZATION = "result/plot.jpg"


class ModelTrainingMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class ModelTraining(metaclass=ModelTrainingMeta):
    def __init__(self):
        self.data = []
        self.labels = []
        self.imagePaths = list(paths.list_images(DATASET_NAME))
        self.testing_splits = None
        self.model = None
        self.data_augmentation = None
        self.label_binarizer = LabelBinarizer()

    @staticmethod
    def create_plot(trained_model) -> None:
        print(f"{get_actual_time()} [INFO] saving model plot...")
        plt.style.use("ggplot")
        plt.figure()
        plt.plot(trained_model.history["loss"], label="train_loss")
        plt.plot(trained_model.history["val_loss"], label="val_loss")
        plt.plot(trained_model.history["accuracy"], label="train_acc")
        plt.plot(trained_model.history["val_accuracy"], label="val_acc")
        plt.title("Training Loss and Accuracy")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss/Accuracy")
        plt.legend(loc="lower left")
        plt.savefig(PLOT_LOCALIZATION)

    def load_training_dataset(self):
        print(f"{get_actual_time()} [INFO] - loading images...")

        for imagePath in progress_bar(self.imagePaths):
            label = imagePath.split(os.path.sep)[-2]
            image = load_img(imagePath, target_size=IMAGE_SIZE)
            image = img_to_array(image)
            image = preprocess_input(image)
            self.data.append(image)
            self.labels.append(label)

        print(f"\n{get_actual_time()} [INFO] - convert images...")
        self.data = np.array(self.data, dtype="float")
        self.labels = np.array(self.labels)

        self.labels = self.label_binarizer.fit_transform(self.labels)
        self.labels = to_categorical(self.labels)

    def prepare_dataset_to_testing_splits(self):
        print(f"{get_actual_time()} [INFO] - split images...")
        self.testing_splits = train_test_split(self.data, self.labels, test_size=0.20,
                                               stratify=self.labels, random_state=42)

        self.data_augmentation = ImageDataGenerator(
            rotation_range=20,
            zoom_range=0.15,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.15,
            horizontal_flip=True,
            fill_mode="nearest")

    def prepareToTraining(self):
        print(f"{get_actual_time()} [INFO] - prepare to training...")
        self.model = Sequential([
            Conv2D(filters=32, kernel_size=(3, 3), padding="same", activation="relu", input_shape=(224, 224, 3)),
            BatchNormalization(),
            MaxPooling2D(pool_size=2),

            Conv2D(filters=64, kernel_size=(3, 3), padding="same", activation="relu"),
            BatchNormalization(),
            MaxPooling2D(pool_size=2),

            Conv2D(filters=128, kernel_size=(3, 3), padding="same", activation="relu"),
            BatchNormalization(),
            MaxPooling2D(pool_size=2),

            Flatten(),
            BatchNormalization(),
            Dense(256, activation="relu"),
            Dropout(0.5),
            BatchNormalization(),
            Dense(512, activation="relu"),
            Dropout(0.5),
            Dense(2, activation='softmax')
        ])

        self.compile_model()

        print(f"{get_actual_time()}: [INFO] summary model...")
        self.model.summary()

    def compile_model(self):
        print(f"{get_actual_time()} [INFO] compiling model...")
        optimizer = Adam(learning_rate=INITIAL_LEARNING_RATE, decay=INITIAL_LEARNING_RATE / EPOCHS)
        self.model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["accuracy"])

    def estimate(self, testX, testY):
        print(f"{get_actual_time()} [INFO] evaluating network...")
        prediction = self.model.predict(testX, batch_size=BATCH_SIZE)
        prediction = np.argmax(prediction, axis=1)
        print(classification_report(testY.argmax(axis=1), prediction, target_names=self.label_binarizer.classes_))

    def save_model(self):
        print(f"{get_actual_time()} [INFO] saving mask detector model...")
        self.model.save(TRAINED_MODEL_LOCALIZATION, save_format="h5")

    def training(self):
        self.load_training_dataset()
        self.prepare_dataset_to_testing_splits()
        self.prepareToTraining()

        (trainX, testX, trainY, testY) = self.testing_splits

        print(f"{get_actual_time()}: [INFO] Data training size: ", len(trainX), "Test data size: ", len(testX))
        print(f"{get_actual_time()} [INFO] training head...")

        early_stopping = EarlyStopping(monitor="val_loss", patience=3, mode="min")

        start = time.time()
        trained_model = self.model.fit(
            self.data_augmentation.flow(trainX, trainY, batch_size=BATCH_SIZE),
            epochs=EPOCHS,
            steps_per_epoch=len(trainX) // BATCH_SIZE,
            validation_data=(testX, testY),
            validation_steps=len(testX) // BATCH_SIZE,
            callbacks=early_stopping)

        end = time.time()
        print(f"{get_actual_time()} [INFO] Total training time: {round((end - start) / 60, 2)} mins")

        self.estimate(testX, testY)
        self.save_model()
        self.create_plot(trained_model)


def get_actual_time():
    now = datetime.now()
    return now.strftime("%H:%M:%S")


network = ModelTraining()
network.training()
