import cv2
import numpy as np
import os
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from Exceptions import FaceMaskException, ImageException


class DetectMaskFromImage:
    def __init__(self):
        CASCADE_PATH = os.path.dirname(cv2.__file__) + "/data/haarcascade_frontalface_alt.xml"
        self.cascade_classifier = cv2.CascadeClassifier(CASCADE_PATH)
        self.model_path = "result/trained.colab21.model"

    def detect_mask(self, image_path: str, model_path: str):
        image = cv2.imread(str(image_path))
        if image is None:
            raise ImageException(f'Error loading image from {image_path}')

        if model_path:
            model = load_model(model_path)
        else:
            model = load_model(self.model_path)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = self.cascade_classifier.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5,
                                                         minSize=(60, 60), flags=cv2.CASCADE_SCALE_IMAGE)

        if len(faces) == 0:
            raise FaceMaskException("There is no faces on image. Try again ")
        found_faces = []
        for (x, y, w, h) in faces:
            face_frame = image[y:y + h, x:x + w]
            face_frame = cv2.cvtColor(face_frame, cv2.COLOR_BGR2RGB)
            face_frame = cv2.resize(face_frame, (224, 224))
            face_frame = img_to_array(face_frame)
            face_frame = np.expand_dims(face_frame, axis=0)
            face_frame = preprocess_input(face_frame)
            found_faces.append(face_frame)

            if 0 < len(found_faces) < 2:
                predictions = model.predict(found_faces)
                for pred in predictions:
                    (mask, withoutMask) = pred
                    label = "Mask" if mask > withoutMask else "No mask"
                    color = (0, 255, 0) if label == "Mask" else (0, 0, 255)
                    label = f"{label}: {max(mask, withoutMask) * 100:.2f}%"
                    cv2.putText(image, label, (x, y - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)

                    cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)

        cv2.imshow("Detect face mask from photo", image)
        cv2.waitKey(0) & ord("q")
